package com.pelagatti.instacart.controller;

import com.pelagatti.instacart.model.Applicant;
import com.pelagatti.instacart.repository.ApplicantRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/applicant")
public class ApplicantController {
    
    private static final int MISSING_PARAMETER_ERROR_CODE = 422;
    
    private final ApplicantRepository repository;

    public ApplicantController(ApplicantRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/")
    public ResponseEntity<Applicant> getByEmail(@RequestParam(value="email") String email) {
        Optional<Applicant> applicant = repository.findByEmail(email);
        return applicant
                .map(applicant1 -> new ResponseEntity<>(applicant1, HttpStatus.OK))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Applicant> getById(@PathVariable(value="id") Long id) {
        Optional<Applicant> applicant = repository.findById(id);
        return applicant
                .map(applicant1 -> new ResponseEntity<>(applicant1, HttpStatus.OK))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Applicant> updateApplicant(@PathVariable(value="id") Long id, @RequestBody Applicant updatedApplicant) {
        Optional<Applicant> existingApplicant = repository.findById(id);
        if(updatedApplicant.getEmail()==null)
            return ResponseEntity.status(MISSING_PARAMETER_ERROR_CODE).build();
        if(!existingApplicant.isPresent())
            return ResponseEntity.notFound().build();
        updatedApplicant.setId(id);
        Applicant savedApplicant = repository.save(updatedApplicant);
        return new ResponseEntity<>(savedApplicant, HttpStatus.ACCEPTED);
    }

    @PostMapping("/")
    public ResponseEntity<Applicant> createApplicant(@RequestBody Applicant applicant) {
        if(applicant.getEmail()==null)
            return ResponseEntity.status(MISSING_PARAMETER_ERROR_CODE).build();
        Applicant savedApplicant = repository.save(applicant);
        return new ResponseEntity<>(savedApplicant, HttpStatus.CREATED);
    }
}