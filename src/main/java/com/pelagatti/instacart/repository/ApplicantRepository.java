package com.pelagatti.instacart.repository;

import com.pelagatti.instacart.model.Applicant;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ApplicantRepository extends CrudRepository<Applicant, Long> {
    
    @Query("select a from Applicant a where a.email = :email")
    Optional<Applicant> findByEmail(@Param("email") String email);

}
