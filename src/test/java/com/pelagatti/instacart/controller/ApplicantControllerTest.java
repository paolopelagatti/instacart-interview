package com.pelagatti.instacart.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pelagatti.instacart.model.Applicant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicantControllerTest {
	private static final ObjectMapper MAPPER = new ObjectMapper();
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void getByEmail() throws Exception {
	}

	@Test
	public void getById() throws Exception {
	
	}

	@Test
	public void updateApplicant() throws Exception {
	}

	@Test
	public void createApplicant() throws Exception {
		Applicant applicant = createDefaultApplicant();
		mockMvc.perform(
				post("/")
					.contentType(MediaType.APPLICATION_FORM_URLENCODED)
					.content(MAPPER.writeValueAsString(applicant))
				)
				.andExpect(status().is(201))
				.andExpect(jsonPath("$.email", is("test@email.com")));
	}
	
	
	private Applicant createDefaultApplicant(){
		Applicant applicant = new Applicant();
		applicant.setFirstName    ("First Name");
		applicant.setLastName     ("Last Name");
		applicant.setRegion       ("Region");
		applicant.setPhone        ("Phone");
		applicant.setEmail        ("test@email.com");
		applicant.setPhoneType    ("MOBILE");
		applicant.setSource       ("Source");
		applicant.setOver21       (true);
		applicant.setReason       ("Reason");
		applicant.setWorkflowState("Workflow State");
		return applicant;

	}

}