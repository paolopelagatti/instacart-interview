# Instacart Shopper

Backend for instacart shopper app. 

##Requirement

This server requires to have a running mysql instance on port 3306, 
with an 'instacart' db and a user named 'instacart-user' with no password.
As an alternative, the app will can ready the following environment variables:
- **SPRING_DATASOURCE_URL** e.g.: jdbc:h2:file:~/test
- **SPRING_DATASOURCE_USERNAME** e.g.: user
- **SPRING_DATASOURCE_PASSWORD**: e.g.: password

## Run

````./gradlew bootRun````

#Documentation
When the server runs, to see APIs documentation visit: [swagger ui](http://localhost:8080/swagger-ui.html)
